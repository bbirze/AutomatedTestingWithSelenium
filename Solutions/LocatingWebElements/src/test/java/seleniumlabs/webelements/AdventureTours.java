package seleniumlabs.webelements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AdventureTours {						
	// page URLs in static consts at top of class
	//-------------------------------------------------
	public static final String appURL = "http://localhost:8080/mtours/";
	public static final String flightFindURL = "http://localhost:8080/mtours/reservation.html";

	private WebDriver driver;
	
	// User TestNg @BeforeMethod and @AfterMethod to stay DRY
	//-------------------------------------------------
	@BeforeMethod
	public void init()  {
		System.out.println("in @BeforeMethod init()!");
		driver = new ChromeDriver();
		driver.get(appURL);              
														// assert we are in the right place
		Assert.assertEquals("Welcome: Adventure Tours", driver.getTitle());
		Assert.assertEquals(appURL, driver.getCurrentUrl());
	}
	
	@AfterMethod
	public void tearDown() throws InterruptedException {
		System.out.println("in @AfterMethod tearDown()");
		Thread.sleep(3000);
		driver.quit();
	}

	// Run as many @Test methods as you want
	//-------------------------------------------------
	@Test
	public void bookFlightHard()  {
		System.out.println("Running Test bookFlightHard!");
		
		// On Adventure Tours Home Page
		//===============================
		WebElement name = driver.findElement(By.name("userName"));
		WebElement passwrd = driver.findElement(By.name("password"));
		WebElement signIn = driver.findElement(By.name("submit"));

		name.sendKeys("guest");
		passwrd.sendKeys("adventure");
		signIn.click();               // use a button... the hard way  :~(

		Assert.assertTrue(driver.getCurrentUrl().startsWith(flightFindURL));
		Assert.assertEquals(driver.getTitle(), "Find a Flight: Adventure Tours:");

		// On Flight Finder Page
		//===============================

	}
	
	@Test
	public void bookFlightEasy() {
		System.out.println("Running Test bookFlightEasy!");
		
		// One Adventure Tours Home Page
		//===============================
		WebElement name = driver.findElement(By.name("userName"));
		WebElement passwrd = driver.findElement(By.name("password"));

		name.sendKeys("guest");
		passwrd.sendKeys("adventure");
		name.submit();									// submit on already found element.... easy!

		Assert.assertTrue(driver.getCurrentUrl().startsWith(flightFindURL));
		Assert.assertEquals(driver.getTitle(), "Find a Flight: Adventure Tours:");

		// On Flight Finder Page
		//===============================
	}
	
	// Show failed test, with and without message
	//-------------------------------------------------
	@Test
	public void test3()   {
		System.out.println("Running Test 3!");
		Assert.assertTrue(false, "Fail assertion on purpose");
	}

	@Test
	public void test4()   {
		System.out.println("Running Test 4!");
	}
	
//	@Test
	public void test5()   {
		System.out.println("Running Test 5!");
	}


}
