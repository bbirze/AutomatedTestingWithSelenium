package seleniumlabs.webelements;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PageLinksTest {
	public static final String appURL = "http://localhost:8080/mtours/";
	public static final String homePageTitle = "Welcome: Adventure Tours";
	public static final String underConstruction= "Under Construction: Adventure Tours" ;
	private WebDriver driver;
	
	@Test
	void checkLinksBadly()  {
		System.out.println("\n In checkLinksBadly()");		

		// On Adventure Tours Home Page
		// ===============================
		List<WebElement> links = driver.findElements(By.tagName("a"));
		System.out.println("Links: " + links);
		
		for (WebElement welm :links)  {      
			System.out.print("Testing Link \"" + welm.getText() + "\"");
			welm.click();
			System.out.println("Directs to : " + driver.getTitle());
			
			if (driver.getTitle().equals(underConstruction))  {
				System.out.println("\t Directs to Under Construction.");
			}
			else if (driver.getTitle().equals("404 Not Found"))  {
				System.out.println("\t Link Is Broken");		
			}
			else  {
				System.out.println("\t Link Is Working");
			}
			driver.navigate().back();
		}
	}
	
	@Test
	public void checkLinksCorrectly() {
		System.out.println("\n In checkLinksCorrectly()");		

		// On Adventure Tours Home Page
		// ===============================
		List<WebElement> links = driver.findElements(By.tagName("a"));
		System.out.println("Links: " + links);
				System.out.println("\n In checkLinksCorrectly()");
		
		String[] linkTexts = new String[links.size()];
		int i=0;
		
		for (WebElement welm :links)  {          // avoid StaleElementReferenceException
			linkTexts[i++] =  welm.getText();	 // Doesn't work if using image instead of text!
		}										 // if have influence, put an attribute on all links... 
		for (String linkText : linkTexts)  {
			driver.findElement(By.linkText(linkText)).click();   // get fresh reference to WebElement
		  													// follow the link
			System.out.println("Testing Link \"" + linkText + "\"" + " Directs to : " + driver.getTitle());
			
			if (driver.getTitle().equals(underConstruction))  {
				System.out.println("\t Directs to Under Construction.");
			}
			else if (driver.getTitle().equals("404 Not Found"))  {
				System.out.println("\t Link Is Broken");		
			}
			else  {
				System.out.println("\t Link Is Working");
			}
			driver.navigate().back();
		}
	}

	@BeforeMethod
	public void init()  {
		System.out.println("in @BeforeMethod init()!");
		driver = new ChromeDriver();
		driver.get(appURL);              
														// assert we are in the right place
		Assert.assertEquals(driver.getTitle(), homePageTitle);
	}
	
	@AfterMethod
	public void tearDown() throws InterruptedException {
		System.out.println("in @AfterMethod tearDown()");
		Thread.sleep(3000);
		driver.quit();
	}
}
