package seleniumlabs.pom.pages;

import org.openqa.selenium.WebDriver;

public class FlightFinderPage  extends TopPage {
	static String PageTitle = "Find a Flight: Adventure Tours:";

	
	public FlightFinderPage(WebDriver driver) {
		super(driver);
	}

	public boolean onPage()  {
		return driver.getTitle().equals(PageTitle);
	}

}
