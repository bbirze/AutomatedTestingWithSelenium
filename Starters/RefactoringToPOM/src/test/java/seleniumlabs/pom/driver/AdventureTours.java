package seleniumlabs.pom.driver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import org.testng.annotations.Test;
import org.testng.Assert;
import static org.testng.AssertJUnit.assertEquals;
import java.util.List;

import seleniumlabs.pom.pages.HomePage;
import seleniumlabs.pom.pages.FlightFinderPage;


public class AdventureTours {
	private WebDriver driver;
	private HomePage homePage = null;
	private FlightFinderPage flghtFinderPg = null;
	
	public void buildDriver() {
		// driver = new FirefoxDriver();
		driver = new ChromeDriver();
		Assert.assertNotNull(driver, "Could Not Instantiate Driver");
	}
	
	@Test
	public void bookFlight() throws InterruptedException {
		buildDriver();
		homePage = new HomePage(driver);
		Assert.assertTrue(homePage.loadPage(), "Could not load home page");

		// One Adventure Tours Home Page
		// ===============================
		flghtFinderPg = homePage.login("guest", "adventure");
		Assert.assertTrue(flghtFinderPg.onPage(), "Could not Login to FlightFinder page");
		
		// One Flight Finder Page
		// ===============================
		List<WebElement> radiotripType = driver.findElements(By
				.name("tripType"));

		Select numPassengers = new Select(driver.findElement(By
				.name("passCount")));
		Select fromPort = new Select(driver.findElement(By.name("fromPort")));
		Select fromMonth = new Select(driver.findElement(By.name("fromMonth")));
		Select fromDate = new Select(driver.findElement(By.name("fromDay")));
		Select toPort = new Select(driver.findElement(By.name("toPort")));
		Select toMonth = new Select(driver.findElement(By.name("toMonth")));
		Select toDate = new Select(driver.findElement(By.name("toDay")));
		List<WebElement> radioCategory = driver.findElements(By
				.name("servClass"));
		Select airline = new Select(driver.findElement(By.name("airline")));

		if (radiotripType.get(1).isSelected()) {  // 1-way selected
			radiotripType.get(0).click();         // want round trip
		} else {
			radiotripType.get(0).click();
		}
		numPassengers.selectByValue("2");
		fromPort.selectByValue("Frankfurt");
		fromMonth.selectByValue("11");
		fromDate.selectByValue("20");
		toPort.selectByValue("Paris");
		toMonth.selectByValue("12");
		toDate.selectByValue("1");
		if (radioCategory.get(0).isSelected()
				|| radioCategory.get(1).isSelected()) {
			radioCategory.get(2).click();         // want first class
		} else {
			radioCategory.get(2).click();
		}
		airline.selectByIndex(2);

		radioCategory.get(0).submit();            // on to flight finder page
		assertEquals("Select a Flight: Adventure Tours", driver.getTitle());

		Thread.sleep(2000);                       // wait so you can see what happened

		// One Adventure Tours Flight Selector
		// ==================================
	
		// work on our departure flight, use XPath to get list of out flights, 
		// then loop through them to find the radio button for Unified Airlines
		//
		List<WebElement> depFlights = driver.findElements(By.xpath("//input[@name='outFlight']"));
		Assert.assertNotNull(depFlights, "Couldn't find list of departing flights");

		for (WebElement cell : depFlights) {
			if (cell.getAttribute("type").equals("radio") && cell.getAttribute("value").contains("Unified Airlines")) {
				System.out.println(
						"\nBooking Departure - Cell type: " + cell.getAttribute("type") + "   value: [" + cell.getAttribute("value") + "]");
				cell.click();                     // select the flight
				break;
			}
		}                                
		// work on our return flight,use XPath to find it in one statement
		//
		WebElement uaFlight = driver.findElement(By.xpath("//input[@name='inFlight'][contains(@value, 'Unified Airlines')]"));
		uaFlight.click();

		uaFlight.submit();                        // submit the form
		assertEquals("Passenger Info: Adventure Tours", driver.getTitle());

		Thread.sleep(2000);                       // wait so you can see what happened

		driver.quit();
	}
	
}
