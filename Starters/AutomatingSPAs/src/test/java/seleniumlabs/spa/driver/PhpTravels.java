package seleniumlabs.spa.driver;

import org.testng.Assert;
import org.testng.annotations.Test;

import seleniumlabs.spa.pages.AdminAddUserPage;
import seleniumlabs.spa.pages.AdminUsersPage;
import seleniumlabs.spa.pages.DashboardPage;
import seleniumlabs.spa.pages.LoginPage;

/**
 * Test driver class for the PHP Travels test scripts.
 *    This driver uses the service methods of Page Objects to do its work, 
 *    with no knowledge of the underlying UI technology.  
 *    <p>Responsible for:
 *    <ul>
 *    <li>Knowing what functionality is being tested</li>
 *    <li>Drive the test using Page Objects </li>
 *    <li>Using Page Object return values to do Asserts to ensure the test is running as expected</li>
 *    </ul>
 */
public class PhpTravels {
	private LoginPage      loginPage = null;
	private AdminUsersPage adminUsersPg = null;
	private DashboardPage  dashboardPg = null;
	private AdminAddUserPage newAdminInfoPg = null;

	/**
	 * Test Adding an Administrator User.
	 * <ol>
	 *    <li>log into the PHP Travels admin view</li>
	 *    <li>Add an Admin user</li>
	 *    <li>Log out</li>
	 *    <li>Log back in using the newly added user</li>
	 *    <li>Assert the correct user is logged in</li>
	 *  </ol>
	 */
	@Test
	public void addAdmin() {
		loginPage = new LoginPage();
		Assert.assertTrue(loginPage.automationOK(), "Could Not Instantiate Driver");
		Assert.assertTrue(loginPage.loadPage(), "Could not load admin login page");

		// PHP Travels Login Page
		// ===============================
		dashboardPg =loginPage.login("admin@phptravels.com", "demoadmin");
		//Assert.assertTrue(dashboardPg.onPage(), "Could not Login to PHP Travels as Admin");

		// On Dash board Page
		// ===============================

		// On Admin Users Page
		// ==================================

		// On Add Admin User Info Page
		// ==================================

			
		// Log off and log back in with new User ID
		// ==========================================
		//loginPage.quit();
	}
}
