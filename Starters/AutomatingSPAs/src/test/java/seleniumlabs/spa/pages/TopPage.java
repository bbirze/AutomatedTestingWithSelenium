package seleniumlabs.spa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/** 
 *  TopPage: super class for every swapped in PageObject, or views that are 
 *  dynamically rendered, making it appear as if a new page was loaded. <p>
 *   Main function is to
 *   <ul>
 *  	<li>Manage the WebDriver and a WebDriver Wait instance for subclasses</li>
 *  	<li>Ensure each Page Objects is a singleton</li>
 *  	<li>Manage access to the SideMenuComponent.  Note, the SideMenuComponent is 
 *  		static, always visible, so it does not inherit from the TopPage.</li>
 *   </ul>
 */
public abstract class TopPage {
	// PageObject that are dynamically rendered, or swapped in and out
	//
	static public enum PageName { LoginPage, AdminUsersPage, DashboardPage, AdminAddUserPage };
	static private LoginPage        loginPage = null;
	static private AdminUsersPage   adminUsersPg = null;
	static private DashboardPage    dashboardPg = null;
	static private AdminAddUserPage newAdminInfoPg = null;

	// Side Menu Object
	static private SideMenuComponent sideMenu;
	
	// WebDriver and its WebDriverWait object
	static protected WebDriverWait wait;
	protected WebDriver driver;
		
	/**
	 * normal constructor, takes a WebDriver, ensuring each pageObject 
	 * will be given a WebDriver in its constructor
	 * 
	 * @param webdriver A WebDriver Instance
	 */
	public TopPage(WebDriver webdriver)  {	
		driver = webdriver;
	}
	
	/**
	 * Startup Constructor called from LoginPage when it is instantiated.
	 * No args triggers instantiation of the WebDriver, its Wait object
	 * and the SideMenuComponent 
	 */
	protected TopPage() {
		// driver = new FirefoxDriver();
		// driver = new InternetExplorerDriver();
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 10);
		sideMenu = new SideMenuComponent(driver); 
	}
	/**
	 * WebDriver management method, allows TestDriver to quit from any PageObject
	 */
	public void quit() {
		driver.quit();
		driver = null;		
	}
	/**
	 * Called by LoginPage to allow TopPage to manage the LoginPage instance as a singleton
	 *
	 * @param lpage LoginPage instance
	 */
	protected void setHomePage(LoginPage lpage) {
		loginPage = lpage;
	}
	/** 
	 * Implemented by each PageObject to check the browser state to verify it is on
	 * the PageObject's page.  Called by the Test Driver after going to a new 
	 * page to assert we are on the right page
	 * 
	 * @return true if the browser is on the PageObject's page. Otherwsie returns false
	 */
	public abstract boolean onPage();
	
	/** 
	 * TopPage is responsible for ensuring the pageObjects it manages are singletons.
	 * 		When a pageObject is transitioning to another page, it calls getPage
	 * 		to request the new pageObject. If the new pageObject is null, it is 
	 * 		instantiated and stored so it can be returned on the next request.
	 * @param page The pageObject we want returned.  The PageName enumerations
	 * 				ensures only legal page objects are requested
	 * @param driver The WebDriver, will be passed to the constructor of each
	 * 				pageObject
	 * @return A pageObject instance returned as its SuperClass, TopPage
	 */
	public static TopPage getPage(PageName page, WebDriver driver) {
		
		switch(page)  {
		case LoginPage : 
			if (loginPage == null)  {
				loginPage = new LoginPage();
			}
			System.out.println("going to LoginPage");
			return loginPage;
		case DashboardPage : 
			if (dashboardPg == null)  {
				dashboardPg = new DashboardPage(driver);
			}
			return dashboardPg;
		case AdminUsersPage : 
			if (adminUsersPg == null)  {
				adminUsersPg = new AdminUsersPage(driver);
			}
			System.out.println("going to AdminUsersPage");
			return adminUsersPg;
		case AdminAddUserPage : 
			if (newAdminInfoPg == null)  {
				newAdminInfoPg = new AdminAddUserPage(driver);
			}
			System.out.println("going to AdminAddUserPage");
			return newAdminInfoPg;
		default:
			System.err.println("TopPage.getPage: No PageObject for given page: " +  page);
		}
		return null;
	}
	
	/* --------------------------------------------------------------------------
	 * The SideMenuComponent is static, so it is always available to any logged 
	 * in pageObject.  TopPage manages this instance, providing passthrough methods
	 * so the Test Drive can access SideMenuComponent functionality from any pageObject subclasses
	 **/
	
	/**
	 * SideMenuComponent pass through method, clicks on the side menu Account.Admins link
	 * @return AdminUserPage pageObject
	 */
	public AdminUsersPage selectAccountsAdmins() {
		return sideMenu.selectAccountsAdmins();
	}
	/**
	 * SideMenuComponent pass through method, clicks the Logout link in the top right hand corner 
	 * of the screen
	 * @return LoginPage pageObject
	 */
	public LoginPage logout() {
		return sideMenu.logout();
	}


}
