package seleniumlabs.spa.pages;

import org.openqa.selenium.WebDriver;

/**
 * AdminUsersPage page shows the current administrator user ids for the PHP 
 * Travels administrator view web app.
 */
public class AdminUsersPage extends TopPage {
	static String PageTitle = "Admins Management";

	/**
	 * Call TopPage constructor passing it the given WebDriver instance, then log
	 * for debugging purposes.
	 * 
	 * @param driver a WebDriver instance
	 */
	public AdminUsersPage(WebDriver driver) {
		super(driver);
		System.out.println("new AdminUsersPage");
	}

	/**
	 * Called by Test Driver to see if the browser is showing the correct page.  
	 * Since the page view is dynamically loaded in the existing DOM, we must wait until 
	 * the load finishes. We do this by waiting for the present of an element on our page.
	 * 
	 * @return true if the browser is showing the Administrator User page, otherwise return false
	 */
	public boolean onPage() {
		return driver.getTitle().equals(PageTitle);
	}
}
