package seleniumlabs.spa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

/**
 * AdminAddUserPage page allows us to add a new administrator user id and configure its
 * authorization for the PHP Travels administrator view web app.
 */
public class AdminAddUserPage extends TopPage {
	static String PageTitle = "Add Admin";

	/**
	 * Call TopPage constructor passing it the given WebDriver instance, then log
	 * for debugging purposes.
	 * 
	 * @param driver a WebDriver instance
	 */
	public AdminAddUserPage(WebDriver driver) {
		super(driver);
		System.out.println("new AdminAddUserPage");
	}

	/**
	 * Called by Test Driver to see if the browser is showing the correct page.  
	 * Since the page view is dynamically loaded in the existing DOM, we must wait until 
	 * the load finishes. We do this by waiting for the present of an element on our page.
	 * 
	 * @return true if the browser is showing the Administrator Add User page, otherwise return false
	 */
	public boolean onPage() {
		// Add wait to ensure SPA page has finished loading
		
		return driver.getTitle().equals(PageTitle);
	}

	// Helper Methods
	// Locate different WebElements on page

	private WebElement getFirstName() {
		return driver.findElement(By.name("fname"));
	}
	private WebElement getLastName() {
		return null;
	}
	private WebElement getEmail() {
		return null;
	}
	private WebElement getPass() {
		return null;
	}
	private WebElement getMobileNum() {
		return null;
	}
	private WebElement getAddr1() {
		return null;
	}
	private WebElement getAddr2() {
		return null;
	}
	private Select getCountry() {
		return null;
	}

}
