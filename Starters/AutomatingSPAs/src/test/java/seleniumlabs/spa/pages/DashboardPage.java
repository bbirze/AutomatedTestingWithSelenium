package seleniumlabs.spa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Dashboard page is the main view for the PHP Travels administrator view web app.
 */
public class DashboardPage extends TopPage {
	static final String PageTitle = "Dashboard";

	/**
	 * Call TopPage constructor passing it the given WebDriver instance, then log
	 * for debugging purposes.
	 * 
	 * @param driver a WebDriver instance
	 */
	public DashboardPage(WebDriver driver) {
		super(driver);
		System.out.println("new DashboardPage");
	}

	/**
	 * Called by Test Driver to see if the browser is showing the correct page.  
	 * Since the page view is dynamically loaded in the existing DOM, we must wait until 
	 * the load finishes. We do this by waiting for the present of an element on our page.
	 * 
	 * @return true if the browser is showing the Dashboard page, otherwise return false
	 */
	public boolean onPage()  {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("ACCOUNTS")));
		return driver.getTitle().equals(PageTitle);
	}
}
