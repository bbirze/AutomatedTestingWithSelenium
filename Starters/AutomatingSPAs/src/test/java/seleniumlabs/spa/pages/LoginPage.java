package seleniumlabs.spa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * LoginPage is the home page for the PHP Travels administrator view web app.
 * It is also the only dynamic page object that has a different initialization
 * algorithm with its super class, TopPage.
 * <p>
 * When LoginPage is instantiated, there is no WebDriver instance yet.  So
 * LoginPage constructor calls the TopPage constructor with no parameters.  This
 * causes the TopPage to instantiate a WebDriver instance. 
 * <p>
 * All other TopPage subclasses are given the WebDriver instance as a parameter in  
 * their constructor, which they pass to TopPage.  This ensures there will only be 
 * one instance of WebDriver in the application
 */
public class LoginPage extends TopPage {
	static final String Url = "https://www.phptravels.net/admin";
	static final String HomePageTitle = "Administator Login";
	
	/**
	 * Call TopPage superclass' default constructor so it will instantiate a WebDriver.
	 * Then call TopPage setHomePage method, giving it the LoginPage instance reference.
	 * TopPage will then manage the LoginPage as a singleton, so its constructor will
	 * never be called again. 
	 */
	public LoginPage() {
		super();
		setHomePage(this);		// tell TopPage about us
		System.out.println("new LoginPage");
	}

	/**
	 * Called by Test Driver to ensure initialization completed with no errors.  If we
	 * have a WebDriver instance, we're good to go
	 * 
	 * @return true if we have a WebDriver instance, otherwise return false
	 */
	public boolean automationOK() {
		return driver!=null;
	}

	/**
	 * Called by Test Driver to initialize the first page on startup.  Maximizes the 
	 * Browser window and goes to our home page URL
	 * 
	 * @return true if the browser is showing the login page, otherwise return false
	 */
	public boolean loadPage() {
		driver.manage().window().maximize();
		driver.get(Url);
		return onPage();
	}
	
	/** 
	 * Checks to see if the browser is showing the login page.  Since the page view is 
	 * dynamically loaded in the existing DOM, we must wait until the load finishes.
	 * We do this by waiting for the present of an element on our page.
	 * 
	 * @return true if the browser is showing the login page, otherwise return false
	 */
	@Override
	public boolean onPage()  {			
		wait.until(ExpectedConditions.presenceOfElementLocated(By.name("email")));
		return driver.getTitle().equals(HomePageTitle);
	}
	
	/**
	 * Called by Test Driver to login into the PHP Travels Admin view app.<p>
	 * A successful login will transition us to the app's Dashboard Page, so we need to
	 * return a DashboardPage instance.  TopPage's getPage() method is responsible for 
	 * supplying page instances to all Page Objects
	 * 
	 * @param userID used ID used to log into the app
	 * @param pwd password used to log into the app
	 * @return DashboardPage instance, 
	 */
	public DashboardPage login(String userID, String pwd) {

		// Add your login code here!
		
		return (DashboardPage)getPage(PageName.DashboardPage, driver);   	
	}
}
