package seleniumlabs.spa.pages;

import org.openqa.selenium.WebDriver;

import seleniumlabs.spa.pages.TopPage.PageName;

/** 
 *  Represents the fixed side menu visible from all the pages.
 *  TopPage holds a static reference to SideMenuComponent for
 *  use by all other pages, so there is no duplication of the
 *  code needed to use the side menu. 
 * */
public class SideMenuComponent  {
	private WebDriver driver;

	public SideMenuComponent(WebDriver webdriver) {
		driver = webdriver;
	}

	/**
	 * Clicks on the side menu Account.Admins link
	 * @return AdminUserPage pageObject
	 */
	public AdminUsersPage selectAccountsAdmins() {

		// Add your code here!
		
		return (AdminUsersPage)TopPage.getPage(PageName.AdminUsersPage, driver);   	
	}
	
	/**
	 * Clicks the Logout link in the top right hand corner of the screen. <p>
	 * 	In this case we simplified by putting all static items in the SideMenuComponent.
	 * 	However, as more functionality is added, the top menu could be managed in its own class.
	 * @return LoginPage pageObject
	 */
	public LoginPage logout() {

		// Add your code here!
		
		return (LoginPage)TopPage.getPage(PageName.LoginPage, driver);   	
	}

}
