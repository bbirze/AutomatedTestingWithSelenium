package seleniumlabs.jenkinstest;

import java.util.Set;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Options;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class JenkinsRunTest {
	private WebDriver driver;

	private void printCookies(Options opt) {
		Set<Cookie> cookies = opt.getCookies(); // retrieve website cookies
		System.out.println("Cookie on this website include:");
		for (Cookie c : cookies) {
			System.out.println("\t " + c);
		}
		System.out.println();
	}

	@Test
	public void f() {
		driver.get("http://puppies.herokuapp.com/");   // open browser and navigate
		System.out.println("Displaying page " + driver.getTitle() + "  URL: " + driver.getCurrentUrl());
		
		// Play with WebDriver.Options
		//--------------------------------
		WebDriver.Options options = driver.manage();
														// add cookie to site
		Cookie cook = new Cookie("Daffy", "Duck");
		options.addCookie(cook);
		printCookies(options);
		
		System.out.println("Just my Cookie: " + options.getCookieNamed("Brigitte") + '\n');
		
		options.deleteCookie(cook);
		printCookies(options);

	}
	
	@BeforeTest
	public void beforeTest()  {
		driver = new ChromeDriver();
	}
	
	@AfterTest
	void afterTest()  {
		driver.quit();
	}
}
