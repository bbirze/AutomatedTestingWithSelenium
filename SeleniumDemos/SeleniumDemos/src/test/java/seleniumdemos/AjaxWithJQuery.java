package seleniumdemos;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class AjaxWithJQuery {

	public static void waitForAjaxControls(WebDriver driver) {
		int timeoutInSeconds = 10;
//		Object numAjaxRequests1 = driver.executeScript("return jQuery.active");
		try {
			if (driver instanceof JavascriptExecutor) {
				JavascriptExecutor jsDriver = (JavascriptExecutor)driver;
				for (int i = 0; i < timeoutInSeconds; i++) {
					Object numAjaxRequests = jsDriver.executeScript("return jQuery.active");
																	// return should be a number
					if (numAjaxRequests instanceof Long) {
						Long n = (Long)numAjaxRequests;
						System.out.println("Number of active jquery AJAX controls: " + n);
						if (n.longValue() == 0L)
							break;
					}
					Thread.sleep(1000);
				}
			} else {
				System.out.println("Web driver: " + driver + " can't run javascript.");
			}
		} catch (InterruptedException e) {
			System.out.println(e);
		}
	}

	public static void main(String[] args) throws InterruptedException {

		WebDriver driver =  new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_ajax_callback");
		
															// Dealing with iFrames
		driver.switchTo().frame(driver.findElement(By.id("iframeResult")));

		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='demo']")));
															// Read the text that appears in the AJAX text control. 
															// Click on the AJAX button control. */
		driver.findElement(By.xpath("//button")).click();
															// Wait for the new content to appear in the AJAX text control. 
		By ajax2ndPara = By.xpath("//*[@id='demo']/p[2]");
		Wait<WebDriver> newwait = new FluentWait<WebDriver>(driver)
				.withTimeout(60, TimeUnit.SECONDS)
				.pollingEvery(1, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class);
		
		newwait.until(ExpectedConditions.presenceOfElementLocated(ajax2ndPara));
															
		//waitForAjaxControls(driver);						// alternate method if have JQuery
		
		WebElement secondParagraph = driver.findElement(ajax2ndPara);
		wait.until(ExpectedConditions.visibilityOf(secondParagraph));
															// Get the text from the second paragraph after the AJAX call
		String ajaxTextSecondPara = secondParagraph.getText().trim();		
		String expectedTextInSecondPara = "AJAX is a technique for accessing web servers from a web page.";
															// Verify the second paragraph text must match with the new text. 
		Assert.assertEquals(ajaxTextSecondPara, expectedTextInSecondPara);
		System.out.println("AJax test with Selenium succeeded! ");
		
		Thread.sleep(2);
		
		driver.quit();
	}

}
