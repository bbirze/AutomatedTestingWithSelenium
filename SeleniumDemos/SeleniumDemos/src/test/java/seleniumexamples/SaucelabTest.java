package seleniumexamples;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SaucelabTest {

	private WebDriver driver;
	private URL url;
	private String sauceUserName; 							// use Sauce Lab creds
	private String sauceAccessKey;
	private String testName;

	@BeforeTest
	public void setVariables() throws MalformedURLException {
		url = new URL("http://ondemand.saucelabs.com:80/wd/hub");
		sauceUserName = "SAUCE_USERNAME";                 // use Sauce Lab creds
		sauceAccessKey = "SAUCE_ACCESS_KEY";              // Best Practice, manage externally
		testName = "myTest";							  // used to track and search for test
	}
	
	@Test
	public void selenium3Sauce() throws MalformedURLException {

		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("username", sauceUserName); 			// set creds
		capabilities.setCapability("accessKey", sauceAccessKey);
		capabilities.setCapability("browserName", "Safari"); 			// rest from Platform Config
		capabilities.setCapability("version", "11.1");
		capabilities.setCapability("platform", "Windows 10");
		capabilities.setCapability("build", "MyApp");					// app build name
		capabilities.setCapability("name", "First-SL-Test");			// make test show up in SL Dashboard

		driver = new RemoteWebDriver(url, capabilities); 				// create remote webdriver

		driver.get("http://www.google.com"); 
		WebElement input = driver.findElement(By.name("q"));
		input.sendKeys("selenium");
		WebElement search = driver.findElement(By.name("btnK"));

		search.click();                        
 
		driver.quit();
	}
	
	@Test
	public void selenium4Sauce() throws MalformedURLException {

		ChromeOptions chromeOpts = new ChromeOptions();
		chromeOpts.setExperimentalOption("w3c",  true);					// set to use W3C protocol
		
		MutableCapabilities sauceOpts = new MutableCapabilities();
		sauceOpts.setCapability("name", testName);					
		sauceOpts.setCapability("seleniumVersion", "3.141.59");
		sauceOpts.setCapability("user", sauceUserName);
		sauceOpts.setCapability("accessKey", sauceAccessKey);
		sauceOpts.setCapability("tags", "w3c-chrome-tests");
		
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("browserName", "googlechrome"); 		// rest from Platform Config
		caps.setCapability("browserVersion", "74.0");
		caps.setCapability("platformName", "Windows 10");
		caps.setCapability("sauce:options", sauceOpts);			// bring in Sauce Extension Capabilities


		driver = new RemoteWebDriver(url, caps); 				// create remote webdriver

		driver.get("http://www.google.com"); 
		WebElement input = driver.findElement(By.name("q"));
		input.sendKeys("selenium");
		WebElement search = driver.findElement(By.name("btnK"));

		search.click();                        
 
		driver.quit();
	}

}
